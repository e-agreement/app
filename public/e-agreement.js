//https://github.com/OffenesJena/openpgp.js-examples
var openpgp = window.openpgp;
openpgp.initWorker({ path:'openpgp.worker.min.js' }) // set the relative web worker path
openpgp.config.show_version = false;
openpgp.config.show_comment = false;

var app = new Vue({
    el: '#app',
   data: {
   note: '',
    date: '',
    mail: '',
    file: '',
    encObj: '',
    decObj: '',
    password: '',
    fileName: '',
    fileSize: '',
    fileBase64: '',
  },
  methods: {
    update: function (event) {
        // `event` is the native DOM event
        if (event) {
            app.encObj = "wait..."
            var options, encrypted;

            options = {
                message: openpgp.message.fromText(JSON.stringify({note: this.note, file: this.fileBase64})), // input as Message object
                passwords: [this.password],                                       // multiple passwords possible
                //armor: true                                                             // ASCII armor
            };

            openpgp.encrypt(options).then(ciphertext => {
                encrypted = ciphertext.data; // get raw encrypted packets as Uint8Array
                console.log(encrypted);
                app.encObj = { date: this.date, encrypted: encrypted, mail: this.mail };
                return encrypted;
            })
            .then(async encrypted => {
            options = {
                message: await openpgp.message.readArmored(app.encObj.encrypted), // parse encrypted bytes
                passwords: [this.password],              // decrypt with password

            };

            openpgp.decrypt(options).then(function(plaintext) {
                console.log({ date: app.date, decrypted: JSON.parse(plaintext.data), mail: app.mail })
                app.decObj = { date: app.date, decrypted: JSON.parse(plaintext.data), mail: app.mail } ;
                return plaintext.data // Uint8Array([0x01, 0x01, 0x01])
            });
            })
        }
        },

  /*! Copyright (c) 2016 Naufal Rabbani (http://github.com/BosNaufal)
  * Licensed Under MIT (http://opensource.org/licenses/MIT)
  *
  * Vue File Base64 @ Version 1.0.0
  *
  * refs: https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
  */
    onChange(e){
        // get the files
        let files = e.target.files;
        // Process each file
        var allFiles = []
        for (var i = 0; i < files.length; i++) {
          let file = files[i]
          // Make new FileReader
          let reader = new FileReader()
          // Convert the file to base64 text
          reader.readAsDataURL(file)
          // on reader load somthing...
          reader.onload = () => {
            // Make a fileInfo Object
            let fileInfo = {
              name: file.name,
              type: file.type,
              size: Math.round(file.size / 1000)+' kB',
              base64: reader.result,
              file: file
            }
            // Push it to the state
            allFiles.push(fileInfo)
            // If all files have been proceed
            if(allFiles.length == files.length){
              // Apply Callback function
              if(this.multiple) app.file = allFiles//this.done(allFiles)
              else app.fileName = allFiles[0].name;
                app.fileSize =  allFiles[0].size;// this.done(allFiles[0])
                app.fileBase64=  allFiles[0].base64;
            }
          } // reader.onload
        } // for
      }, // onChange()
    }
})